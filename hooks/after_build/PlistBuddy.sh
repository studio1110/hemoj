#!/bin/bash
PLIST=platforms/ios/*/*-Info.plist
cat << EOF |
Add :NSAppTransportSecurity dict
Add :NSAppTransportSecurity:NSAllowsArbitraryLoads bool YES
Add :UIViewControllerBasedStatusBarAppearance bool FALSE
Add :UIRequiresFullScreen bool YES
Add :UIStatusBarHidden bool YES
EOF
while read line
do
    /usr/libexec/PlistBuddy -c "$line" $PLIST
done

true