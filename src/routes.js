export default [{
        path: "/",
        component: require("main.vue")
    },
    {
        path: "/accueil",
        component: require("accueil.vue")
    },
    {
        path: "/mentions",
        component: require("./assets/vue/pages/mentions.vue")
    },
    {
        path: "/donneespersonnelles",
        component: require("./assets/vue/pages/donneespersonnelles.vue")
    },
    {
        path: "/bonusage",
        component: require("./assets/vue/pages/bonusage.vue")
    },
    {
        path: "/avatar",
        component: require("./assets/vue/pages/avatar.vue")
    },
    {
        path: "/alerte",
        component: require("./assets/vue/pages/alertes/nouvelle.vue")
    },
    {
        path: "/alertes",
        component: require("./assets/vue/pages/alertes/liste.vue")
    },
    {
        path: "/alerte/:alertid",
        component: require("./assets/vue/pages/alertes/edition.vue")
    },
    {
        path: "/historique",
        component: require("./assets/vue/pages/historique/liste.vue")
    },
  {
    path: "/debug",
    component: require("./assets/vue/pages/debug.vue")
  }
];