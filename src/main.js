import Vue                      from "vue";
import Framework7               from "framework7";
import Framework7Vue            from "framework7-vue";
import Framework7Theme          from "framework7/dist/css/framework7.ios.min.css";
import Framework7ThemeColors    from "framework7/dist/css/framework7.ios.colors.min.css";
import AppStyles                from "./assets/sass/main.scss";
import Routes                   from "./routes.js";
import Lodash                   from "lodash";
import Greensock                from "greensock";
import ProgressBar              from "progressbar.js";
import Chartjs                  from "chart.js";
import SwiperJS                 from "swiper/dist/js/swiper.min.js";
import SwiperCSS                from "swiper/dist/css/swiper.min.css";
import Slick                    from "slick";
import jQuery                   from "jquery";
import Datepicker               from "vuejs-datepicker";
import VueTimepicker            from "vue2-timepicker";
import Vue2Animate              from "vue2-animate/dist/vue2-animate.min.css";
import App                      from "./main.vue";
import ValueCircle              from "./assets/vue/components/valuecircle.vue";
import Avatar                   from "./assets/vue/components/avatar.vue";
import PopupInjection           from "./assets/vue/components/popup-injection.vue";
import PopupConditionsAcceptees from "./assets/vue/components/popup-conditions-acceptees.vue";
import PopupCodeEntrer          from "./assets/vue/components/popup-codeentrer.vue";
import PopupCode                from "./assets/vue/components/popup-code.vue";
import PopupCodeLoose           from "./assets/vue/components/popup-codeloose.vue";
import PopupCodeModifier        from "./assets/vue/components/popup-codemodifier.vue";
import PopupCodeLooseModifier   from "./assets/vue/components/popup-codeloosemodifier.vue";
import PopupCodeDesactiver      from "./assets/vue/components/popup-codedesactiver.vue";
import Alertes                  from "./assets/vue/components/alertes.vue";
import F73DPanelJS              from "./assets/js/framework7.3dpanels.min.js";
import F73DPanelCSS             from "./assets/js/framework7.3dpanels.min.css";
import moment                   from "moment";
import momentrecur              from "moment-recur";
import AvatarOff                from "./assets/images/avatar.png";
import customfont1              from "./assets/fonts/AveniLig.ttf";
import customfont2              from "./assets/fonts/AveniMed.ttf";
import customfont3              from "./assets/fonts/AveniHea.ttf";

Vue.use(Framework7Vue);
Vue.component("valuecircle", ValueCircle);
Vue.component("avatar", Avatar);
Vue.component("popup-injection", PopupInjection);
Vue.component("popup-conditions-acceptees", PopupConditionsAcceptees);
Vue.component("popup-codeentrer", PopupCodeEntrer);
Vue.component("popup-code", PopupCode);
Vue.component("popup-codeloose", PopupCodeLoose);
Vue.component("popup-codemodifier", PopupCodeModifier);
Vue.component("popup-codeloosemodifier", PopupCodeLooseModifier);
Vue.component("popup-codedesactiver", PopupCodeDesactiver);
Vue.component("datepicker", Datepicker);
Vue.component("vue-timepicker", VueTimepicker);
Vue.component("alertes", Alertes);

if(typeof Object.assign !== "function") {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value : function assign(target, varArgs) { // .length of function is 2
      "use strict";
      if(target == null) { // TypeError if undefined or null
        throw new TypeError("Cannot convert undefined or null to object");
      }

      var to = Object(target);

      for(var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if(nextSource != null) { // Skip over if undefined or null
          for(var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if(Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable : true,
    configurable : true
  });
}

var bus = new Vue({
  data : function() {
    return {
      regularite : 0,
      ponctualite : 0,
      alertes : [],
      injection : null,
      injections : [],
      recurrences_string : [],
      sounds : [],
      is_index : true,
      avatar_index : -1,
      limit_max : 8,
      limit_type_recur : "day",
      limit_type : "days"
    };
  },
  computed : {
    needCode : function() {
      return this.needCodeVerification();
    },
    resetEmail : function() {
      var code_datas = JSON.parse(window.localStorage.getItem("code"));
      return code_datas && code_datas.email ? code_datas.email : "";
    },
    codeEmailExist : function() {
      var code_datas = JSON.parse(window.localStorage.getItem("code"));
      return code_datas && code_datas.email && code_datas.email.length > 0;
    },
    nombreInjectionsRellementEffectuees : function() {
      return this.regularite;
      var me = this;
      var ret = _.filter(this.injections, {
        done : true
      });
      if(ret) {
        return ret.length;
      }
      return 0;
    },
    nombreInjectionsNonRetardees : function() {
      return this.ponctualite;
      var ret = _.filter(this.injections, {
        done : true,
        late : true
      });
      if(ret) {
        return this.nombreInjectionsFaites - ret.length;
      }
      return 0;
    },
    nombreInjectionsFaites : function() {
      return 100;
      var ret = _.filter(this.injections, {
        done : true
      });
      if(ret) {
        return ret.length;
      }
      return 0;
    },
    nombreInjectionsDues : function() {
      return 100;
      var me = this;
      var injections_totales = 0;
      if(this.alertes.length === 0) {
        this.alertes = this.loadAlertesFromDisk();
      }
      var alertes_en_cours = _.filter(this.alertes, function(a) {
        return a.status === true;
      });
      var alertes_terminees = _.filter(this.alertes, function(a) {
        return a.status !== true;
      });
      _.forEach(alertes_en_cours, function(a) {
        if(moment(a.recurrence.startdate, "YYYY-MM-DD").isBefore()) {
          var injections_soustotal = moment().recur({
            start : a.recurrence.startdate,
            end : moment()
          }).every(a.recurrence.times, me.limit_type_recur).all().length;
          injections_totales += injections_soustotal;
        }
      });
      _.forEach(alertes_terminees, function(a) {
        if(moment(a.recurrence.startdate, "YYYY-MM-DD").isBefore()) {
          var injections_soustotal = moment().recur({
            start : a.recurrence.startdate,
            end : moment(a.date_fin, "YYYY-MM-DD")
          }).every(a.recurrence.times, me.limit_type_recur).all().length;
          injections_totales += injections_soustotal;
        }
      });
      return injections_totales;
    },
    avatars : function() {
      var avatars = [];
      for(var i = 1; i <= 18; i++) {
        avatars.push({
          src : "./static/images/avatars/av" + i + ".png"
        });
      }
      return avatars;
    }
  },
  methods : {
    reloadSounds : function(callback) {
      callback(_.sortBy([{
        Name : "Aurora",
        Url : "file://static/notifications_tones/aurora.mp3"
      },
        {
          Name : "Bamboo",
          Url : "file://static/notifications_tones/bamboo.mp3"
        },
        {
          Name : "Chord",
          Url : "file://static/notifications_tones/chord.mp3"
        },
        {
          Name : "Complete",
          Url : "file://static/notifications_tones/complete.mp3"
        },
        {
          Name : "Hello",
          Url : "file://static/notifications_tones/hello.mp3"
        },
        {
          Name : "Input",
          Url : "file://static/notifications_tones/input.mp3"
        },
        {
          Name : "Keys",
          Url : "file://static/notifications_tones/keys.mp3"
        },
        {
          Name : "Note",
          Url : "file://static/notifications_tones/note.mp3"
        },
        {
          Name : "Popcorn",
          Url : "file://static/notifications_tones/popcorn.mp3"
        },
        {
          Name : "Pulse",
          Url : "file://static/notifications_tones/pulse.mp3"
        },
        {
          Name : "Synth",
          Url : "file://static/notifications_tones/synth.mp3"
        },
        {
          Name : "Apex",
          Url : "file://static/ringtones/apex.mp3"
        },
        {
          Name : "Beacon",
          Url : "file://static/ringtones/beacon.mp3"
        },
        {
          Name : "Bulletin",
          Url : "file://static/ringtones/bulletin.mp3"
        },
        {
          Name : "Cosmic",
          Url : "file://static/ringtones/cosmic.mp3"
        },
        {
          Name : "Crystals",
          Url : "file://static/ringtones/crystals.mp3"
        },
        {
          Name : "Hillside",
          Url : "file://static/ringtones/hillside.mp3"
        },
        {
          Name : "Illuminate",
          Url : "file://static/ringtones/illuminate.mp3"
        },
        {
          Name : "Opening",
          Url : "file://static/ringtones/opening.mp3"
        },
        {
          Name : "Presto",
          Url : "file://static/ringtones/presto.mp3"
        },
        {
          Name : "Radar",
          Url : "file://static/ringtones/radar.mp3"
        },
        {
          Name : "Signal",
          Url : "file://static/ringtones/signal.mp3"
        },
        {
          Name : "Stargaze",
          Url : "file://static/ringtones/stargaze.mp3"
        }], "Name"));
    },
    loadAvatar : function() {
      if(window.localStorage.getItem("cslavatar")) {
        this.avatar_index = window.localStorage.getItem("cslavatar");
        return this.avatars[this.avatar_index].src.replace(/av([0-9]+)/, "gav$1");
      }
      else {
        return AvatarOff;
      }
    },
    saveAvatar : function(i) {
      this.avatar_index = i;
      window.localStorage.setItem("cslavatar", i);
    },
    addRegularite : function() {
      if(!window.localStorage.getItem("regularite")) {
        window.localStorage.setItem("regularite", 0);
      }
      var regularite = 1 * window.localStorage.getItem("regularite");
      if(regularite + 25 <= 100) {
        window.localStorage.setItem("regularite", regularite += 25);
      }
      else {
        window.localStorage.setItem("regularite", 100);
      }
      this.regularite = 1 * window.localStorage.getItem("regularite");
    },
    removeRegularite : function() {
      if(!window.localStorage.getItem("regularite")) {
        window.localStorage.setItem("regularite", 0);
      }
      var regularite = 1 * window.localStorage.getItem("regularite");
      if(regularite - 25 >= 0) {
        window.localStorage.setItem("regularite", regularite -= 25);
      }
      else {
        window.localStorage.setItem("regularite", 0);
      }
      this.regularite = 1 * window.localStorage.getItem("regularite");
    },
    addPonctualite : function() {
      if(!window.localStorage.getItem("ponctualite")) {
        window.localStorage.setItem("ponctualite", 0);
      }
      var ponctualite = 1 * window.localStorage.getItem("ponctualite");
      if(ponctualite + 25 <= 100) {
        window.localStorage.setItem("ponctualite", ponctualite += 25);
      }
      else {
        window.localStorage.setItem("ponctualite", 100);
      }
      this.ponctualite = 1 * window.localStorage.getItem("ponctualite");
    },
    removePonctualite : function() {
      if(!window.localStorage.getItem("ponctualite")) {
        window.localStorage.setItem("ponctualite", 0);
      }
      var ponctualite = 1 * window.localStorage.getItem("ponctualite");
      if(ponctualite - 25 >= 0) {
        window.localStorage.setItem("ponctualite", ponctualite -= 25);
      }
      else {
        window.localStorage.setItem("ponctualite", 0);
      }
      this.ponctualite = 1 * window.localStorage.getItem("ponctualite");
    },
    isNewAlertOk : function(d) {
      var thedate = moment(d.a_date + " " + d.a_time, "YYYY-MM-DD hh:mm");
      var cond = thedate.isAfter(moment().add(1,"minute"));
      if(!cond) {
        return {
          s : "a_date",
          m : "Sélectionner une date postérieure"
        };
      }
      if(d.a_recurrence < 0) {
        return {
          s : "a_recurrence",
          m : "Sélectionner la récurrence"
        };
      }
      if(d.a_sound < 0) {
        return {
          s : "a_sound",
          m : "Sélectionner une sonnerie"
        };
      }
      if(d.a_label.length <= 2) {
        return {
          s : "a_label",
          m : "Nom trop court"
        };
      }
      return true;
    },
    isUpdateAlertOk : function(d) {
      if(d.a_sound < 0) {
        return {
          s : "a_sound",
          m : "Sélectionner une sonnerie"
        };
      }
      if(d.a_label.length <= 2) {
        return {
          s : "a_label",
          m : "Nom trop court"
        };
      }
      return true;
    },
    addNewAlert : function(d) {
      var new_alert = {
        id : "" + moment().valueOf(),
        label : d.a_label,
        date_fin : null,
        recurrence : {
          startdate : moment(d.a_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
          times : d.a_recurrence,
          kind : "days"
        },
        status : true,
        time : d.a_time,
        sound : (d.a_sound && d.a_sound.length) > 0 ? d.a_sound : "default"
      };
      if(this.alertes === null) {
        this.alertes = this.loadAlertesFromDisk();
      }
      this.alertes.push(new_alert);
      this.saveAlertesToDisk(this.alertes);
      this.programNotifications(new_alert);
    },
    updateAlert : function(id, d) {
      var me = this;
      var an_alert = {
        id : id,
        label : d.a_label,
        // recurrence : {
        //   startdate : moment(d.a_date, "YYYY-MM-DD").format("YYYY-MM-DD"),
        //   times : d.a_recurrence,
        //   kind : "days"
        // },
        // status : true,
        // time : d.a_time,
        sound : d.a_sound
      };
      if(this.alertes === null) {
        this.alertes = this.loadAlertesFromDisk();
      }
      var idx = _.findIndex(this.alertes, {
        "id" : "" + id
      });
      this.alertes[idx].label = an_alert.label;
      this.alertes[idx].sound = an_alert.sound;
      this.saveAlertesToDisk(this.alertes);
      this.removeNotifications(id, function() {
        me.programNotifications(an_alert);
      });
    },
    deleteAlert : function(id, cb_ok, cb_error) {
      var me = this;
      if(this.alertes === null) {
        this.alertes = this.loadAlertesFromDisk();
      }
      var alerte = _.find(this.alertes, function(o) {
        return "" + o.id === "" + id;
      });
      var injection_de_alerte = _.find(this.injections, function(i) {
        return "" + i.alertid === "" + id;
      });
      if(alerte) {
        if(injection_de_alerte !== undefined) {
          alerte.status = false;
          alerte.date_fin = new Date();
        }
        else {
          _.remove(this.alertes, {
            "id" : id
          });
        }
        this.saveAlertesToDisk(this.alertes);
        this.alertes = this.loadAlertesFromDisk();
        this.removeNotifications(id, cb_ok);
      }
      else {
        cb_error();
      }
    },
    saveAlertesToDisk : function(alertes) {
      window.localStorage.setItem("cslalerte_alertes", JSON.stringify(alertes));
    },
    loadAlertesFromDisk : function() {
      if(window.localStorage.getItem("cslalerte_alertes")) {
        return JSON.parse(window.localStorage.getItem("cslalerte_alertes"));
      }
      else {
        return [];
      }
    },
    removeNotifications : function(id, cb) {
      var thereturn = _.once(cb);
      if(!cordova.plugins.notification) {
        thereturn();
      }
      else {
        cordova.plugins.notification.local.getScheduled(function(notifications) {
          var subnotifications = _.filter(notifications, function(n) {
            var data = JSON.parse(n.data);
            return 1 * id === 1 * data.alertid;
          });
          var subids = [];
          _.each(subnotifications, function(s) {
            subids.push(s.id);
          });
          cordova.plugins.notification.local.cancel(subids, function() {
            thereturn();
          });
        });
      }
    },
    programNotifications : function(alert, force) {
      var me = this;
      var at_hour = alert.time.match(/(.*):(.*)/)[1];
      var at_minute = alert.time.match(/(.*):(.*)/)[2];
      var startdateandtime = moment(alert.recurrence.startdate, "YYYY-MM-DD").hour(at_hour).minute(at_minute).second(0).millisecond(0);

      if(startdateandtime.isBefore(moment(), me.limit_type) || force === true) {
        while(startdateandtime.isBefore(moment(), me.limit_type)) {
          startdateandtime = startdateandtime.add(alert.recurrence.times * 1, me.limit_type);
        }
        if(force!==true) startdateandtime = startdateandtime.add(alert.recurrence.times * 1, me.limit_type);
      }

      var to_schedule = [];

      if(alert.recurrence.times >= 1) {
        for(var i = 0; i < me.limit_max; i++) {
          var n = startdateandtime.clone().add(alert.recurrence.times * i, me.limit_type);
          if(n) {
            var topush = {
              id : 1 * n.valueOf() + 1 * alert.id,
              title : "HémoJ",
              text : alert.label,
              trigger : {
                at : n.toDate().getTime()
              },
              sound : alert.sound,
              data : {
                "alertid" : alert.id,
                "sound" : alert.sound,
                "nextdate" : startdateandtime.clone().add(alert.recurrence.times * (i + 1), me.limit_type)
              }
            };
            to_schedule.push(topush);
          }
        }
        cordova.plugins.notification.local.schedule(to_schedule);
      }
    },
    reprogramNotifications : function() {
      var me = this;
      _.each(this.alertes, function(al) {
        if(al.status !== false) {
          me.removeNotifications(al.id, function() {
            me.programNotifications(al,true);
          });
        }
      });
    },
    manageNotificationEvents : function() {
      console.log("MANAGE NOTIFICATION EVENTS");
      var me = this;
      cordova.plugins.notification.local.on("add", function(notification) {
      });
      cordova.plugins.notification.local.on("trigger", function(notification) {
        me.showToastNotification(notification);
        _.delay(feedback, 500);
      });
      cordova.plugins.notification.local.on("click", function(notification) {
        _.delay(feedback, 500);
      });

      function feedback() {
        me.reprogramNotifications();
      }

      _.delay(feedback, 500);
    },
    traceNotifications : function() {
      _.delay(function() {
        cordova.plugins.notification.local.getScheduled(function(notifications) {
          console.log("====================================");
          console.log("scheduled");
          _.map(notifications, function(n) {
            if(n.trigger && n.trigger.at) {
              var t = moment(n.trigger.at * 1000).format();
              console.log(n.text + ":" + t);
            }
            else {
              console.log(n);
            }
          });
          console.log("====================================");
        });
      }, 3000);
    },
    showToastNotification : function(notification) {
      var me = this;
      var dataofnotification = notification.data;
      if(null !== dataofnotification.sound) {
        cordova.plugins.NativeRingtones.playRingtone(dataofnotification.sound);
      }
      var thealert = _.find(this.alertes, {
        "id" : dataofnotification.alertid
      });
      if(thealert) {
        var themessage = moment(thealert.recurrence.startdate).format("YYYY-MM-DD") + " - " + thealert.time + " : " + thealert.label;
      }
      var t = window.f7.addNotification({
        title : notification.title,
        subtitle : notification.text,
        hold : 10 * 1000,
        message : themessage,
        onClose : function() {
        },
        onClick : function() {
          me.showInjectionForNotification(notification);
          window.f7.closeNotification(".notifications");
        }
      });

    },
    showInjectionForNotification : function(notification) {
    },
    isInjectionBreakStreak : function(injections, injection) {
      if(!injections) {
        return false;
      }
      var alerte = _.find(this.alertes, {
        "id" : injection.alertid
      });
      var last_injection = _.head(_.orderBy(_.filter(injections, {"alertid" : injection.alertid}), ["date"], ["desc"]));
      if(last_injection !== undefined && last_injection.done === true) {
        var today = moment();
        var last_day = moment(_.clone(last_injection.date));
        var injection_day_after_last = moment(_.clone(last_injection.date)).add(alerte.recurrence.times, "days");
        var compare = last_day.isSameOrBefore(moment(), "day") && moment().isSameOrBefore(injection_day_after_last, "day");
        // var compare = moment().isBetween(last_day, injection_day_after_last, "hour");
        return !compare;
      }
      return false;
    },
    isInjectionTooLate : function(alert, today) {
      var AM1 = moment(alert).clone().subtract(1, "hours");
      var AP1 = moment(alert).clone().add(1, "hours");
      // A-1h (c1) <= T <= A+1h (c2)
      var C1 = moment(today).isSameOrAfter(AM1, "minute");
      var C2 = moment(today).isSameOrBefore(AP1, "minute");
      var r = !(C1 && C2);
      return r;
    },
    saveInjectionDatas : function(produit, lot, after) {
      // alertid: "1520191631768"
      // date: "2018-03-04"
      // done: true
      // id: "1520191631768-03/04/2018"
      // label: "AAA"
      // late: true
      // lot: "NA"
      // produit: "NA"
      // show: true
      // time: "20:30"
      // Prototype Object
      // alertid: "1520191689833"
      // date: "2018-03-04"
      // done: true
      // id: "1520191689833-03/04/2018"
      // label: "BBB"
      // late: true
      // lot: "NA"
      // produit: "NA"
      // show: true
      // time: "20:30"

      var shouldbeat = moment(this.injection.date + " " + this.injection.time, "YYYY-MM-DD hh:mm");
      shouldbeat = moment(this.injection.date + " " + this.injection.time, "MM/DD/YYYY hh:mm");
      var me = this;
      this.injection.date = moment().format("YYYY-MM-DD");
      this.injection.time = moment().format("HH:mm");
      this.injection.produit = produit;
      this.injection.lot = lot;
      this.injection.done = true;
      this.injection.late = this.isInjectionTooLate(shouldbeat.toDate(), new Date());
      if(this.injection.late) {
        this.removePonctualite();
      }
      else {
        this.addPonctualite();
      }
      if(this.isInjectionBreakStreak(this.injections, this.injection)) {
        this.removeRegularite();
      }
      else {
        this.addRegularite();
      }

      if(!me.injections) {
        if(window.localStorage.getItem("cslinjections")) {
          me.injections = JSON.parse(window.localStorage.getItem("cslinjections"));
        }
        else {
          me.injections = [];
        }
      }

      if(!_.find(me.injections, {
          "id" : me.injection.id
        })) {
        me.injections.push(me.injection);
      }
      me.reprogramNotifications();
      window.localStorage.setItem("cslinjections", JSON.stringify(me.injections));
      after();
    },
    deleteInjection : function(id, cb_ok, cb_error) {
      var me = this;
      if(me.injections === null) {
        me.injections = JSON.parse(window.localStorage.getItem("cslinjections"));
      }
      if(_.find(me.injections, {
          "id" : id
        })) {
        var suppressed = _.remove(me.injections, function(o) {
          return "" + o.id === "" + id;
        });
        if(suppressed[0].id !== id) {
          cb_error();
        }
        else {
          window.localStorage.setItem("cslinjections", JSON.stringify(me.injections));
          me.removePonctualite();
          me.removeRegularite();
          if(me.injections.length === 0) {
            me.injection = null;
            me.injections = [];
            window.localStorage.removeItem("cslinjections");
            window.localStorage.setItem("regularite", 0);
            window.localStorage.setItem("ponctualite", 0);
          }
          cb_ok();
        }
      }
      else {
        cb_error();
      }
    },
    isTemporaryCode : function() {
      return !!window.localStorage.getItem("forcecode");
    },
    needCodeVerification : function() {
      if(window.localStorage.getItem("code")) {
        var code_datas = JSON.parse(window.localStorage.getItem("code"));
        return code_datas && code_datas.code.length ? code_datas.code.length > 0 : false;
      }
      return false;
    },
    isCodeOk : function(code) {
      var code_datas = JSON.parse(window.localStorage.getItem("code"));
      return code === code_datas.code;
    },
    saveCode : function(datas) {
      window.localStorage.setItem("code", JSON.stringify(datas));
      window.localStorage.removeItem("forcecode");
    },
    resetCodeTryout : function() {
      this.code_tryout = null;
    },
    deleteCode : function() {
      window.localStorage.removeItem("code");
    },
    makeNewCode : function(d) {
      var newd = new String(btoa(d.slice(0, 8))).slice(0, 8);
      return newd;
    },
    removeEmailForCode : function() {
      var code_datas = JSON.parse(window.localStorage.getItem("code"));
      code_datas.email = "";
      window.localStorage.setItem("code", JSON.stringify(code_datas));
    },
    sendMyCode : function() {
      var code_datas = JSON.parse(window.localStorage.getItem("code"));
      code_datas.code = bus.makeNewCode(code_datas.email);
      window.localStorage.setItem("code", JSON.stringify(code_datas));
      window.localStorage.setItem("forcecode", 1);
      if(code_datas.email && code_datas.email.length > 0) {
        jQuery.ajax({
          url : "https://hemoj.jake-digital.com/ljyfdgihhlkgfyogoogjyd/mdppnotif.php",
          method : "POST",
          data : code_datas
        });
        return true;
      }
      return false;
    },
    needResetBecauseOfCode : function() {
      this.code_tryout = this.code_tryout ? this.code_tryout + 1 : 1;
      return this.code_tryout >= 4;
    },
    getCodeEnterLeft : function() {
      var msgs = ["Code erroné, il vous reste 3 essais...",
                  "Code erroné, il vous reste 2 essais...",
                  "Ceci est votre dernier essai : en cas d'erreur les données seront effacées."];
      return msgs[this.code_tryout - 1];
    },
    resetApp : function() {
      var me = this;
      window.f7.showIndicator();
      cordova.plugins.notification.local.cancelAll(function() {
      }, this);
      window.localStorage.removeItem("code");
      window.localStorage.removeItem("forcecode");
      window.localStorage.removeItem("nopopupatstart");
      window.localStorage.removeItem("cslalerte_alertes");
      window.localStorage.removeItem("cslavatar");
      window.localStorage.removeItem("cslinjections");
      window.localStorage.removeItem("ponctualite");
      window.localStorage.removeItem("regularite");
      this.code_tryout = null;
      this.alertes = [];
      this.injection = null;
      this.injections = [];
      this.is_index = true;
      this.regularite = 0;
      this.ponctualite = 0;
      this.avatar_index = -1;
      _.delay(function() {
        window.f7.hideIndicator();
        me.$f7['mainView'].loadPage('/accueil/')
        window.f7.mainView.router.loadPage("/accueil/");
      }, 1000)

    },
    create : function(go_on) {
      var me = this;
      if(me.recurrences_string.length === 0) {
        me.recurrences_string.push({
          id : 1,
          label : "Tous les jours",
          v : [1,
               "days"]
        });
        for(var i = 2; i <= 14; i++) {
          me.recurrences_string.push({
            id : i,
            label : "Tous les " + i + " jours",
            v : [i,
                 "days"]
          });
        }
        me.reloadSounds(function(s) {
          me.sounds = s;
          me.alertes = me.loadAlertesFromDisk();
          if(me.alertes.length > 0) {
            if(undefined !== window.localStorage.getItem("cslinjections")) {
              me.injections = JSON.parse(window.localStorage.getItem("cslinjections"));
            }
            else {
              me.injections = [];
            }
          }
          if(window.localStorage.getItem("regularite")) {
            me.regularite = window.localStorage.getItem("regularite");
          }
          if(window.localStorage.getItem("ponctualite")) {
            me.ponctualite = window.localStorage.getItem("ponctualite");
          }
          cordova.plugins.notification.local.hasPermission(function(granted) {
            if(granted) {
              me.manageNotificationEvents(me);

              if(typeof (go_on) === "function") {
                go_on();
              }
            }
            else {
              cordova.plugins.notification.local.requestPermission(function(granted) {
                if(granted) {
                  me.manageNotificationEvents(me);

                  if(typeof (go_on) === "function") {
                    go_on();
                  }
                }
                else {
                  navigator.notification.alert("Impossible d'enregistrer les notifications :(");
                }
              });
            }
          });
        });
      }
      else {
        if(typeof (go_on) === "function") {
          go_on();
        }
      }
    }
  }
});

Vue.mixin({
  filters : {
    formatdate : function(value) {
      if(/([0-9]+)\//.test(value)) {
        return moment(value, "MM/DD/YYYY").format("DD-MM-YYYY");
      }
      else {
        return moment(value, "YYYY-MM-DD").format("DD-MM-YYYY");
      }
    }
  }
});

export default bus;

var alreadystarted = false;

function start() {
  if(alreadystarted === true) {
    return;
  }
  alreadystarted = true;
  if(cordova && cordova.platformId == "android") {
    _.delay(function() {
      // window.AndroidFullScreen.immersiveMode(false, false);
      window.addEventListener("native.keyboardshow", function(e) {
        window.AndroidFullScreen.showSystemUI(false, false);
        // TweenMax.to(".pages", 0.25, { y: "-=120" });
      });
      window.addEventListener("native.keyboardhide", function(e) {
        window.AndroidFullScreen.immersiveMode(false, false);
        // TweenMax.to(".pages", 0.25, { y: "+=120" });
      });
      cordova.plugins.Keyboard.disableScroll(false);
    }, 1000);
  }
  // Init App
  new Vue({
    el : "#app",
    template : "<app/>", // Init Framework7 by passing parameters here
    framework7 : {
      activeState : false,
      activeStateElements : "",
      swipeBackPage : false,
      tapHoldPreventClicks : false,
      sortable : false,
      modalTitle : "HémoJ",
      modalPreloaderTitle : "Chargement...",
      modalButtonOk : "Ok",
      modalButtonCancel : "Annuler",
      statusbarOverlay : false,
      root : "#app", /* Uncomment to enable Material theme: */
      // material: true,
      routes : Routes
    }, // Register App Component
    components : {
      app : App
    },
  });
}

document.addEventListener("deviceready", function() {
  start();
});

_.delay(function() {
  start();
}, 5000);

